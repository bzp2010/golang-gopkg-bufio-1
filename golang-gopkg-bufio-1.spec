%global goreponame      bufio
%global goipath         github.com/go-bufio/bufio

%gometa

%global common_description add ReadN to golang.org/pkg/bufio/.

%global golicenses      LICENSE
%global godocs          README.md

Name:           %{goname}
Version:        1
Release:        1
Summary:        %{common_description}

License:        MIT
URL:            https://%{goipath}
Source0:        https://%{goipath}/archive/v%{version}/%{goreponame}-%{version}.tar.gz

%description
%{common_description}

%package devel
Summary:       %{common_description}
BuildArch:     noarch

%description devel
This package contains library source intended for building other packages,
which use import path with %{common_description} prefix.

%prep
%setup -n %{goreponame}-%{version}

%install
mkdir -p %{buildroot}%{gopath}/src/%{goipath}
cp -r ./* %{buildroot}%{gopath}/src/%{goipath}
chmod 755 %{buildroot}%{gopath}/src/%{goipath}/*
%files devel
%doc %{golicenses} %{godocs}
%{gopath}/src/%{goipath}

%changelog
* Sat Aug 28 2021 bzp2010 <1286886774@qq.com> - 0-1
- Initial package
